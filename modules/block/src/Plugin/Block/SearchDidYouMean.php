<?php

declare(strict_types=1);

namespace Drupal\search_web_components_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;

/**
 * Provides a did you mean spellcheck block.
 *
 * @Block(
 *   id = "swc_search_did_you_mean",
 *   admin_label = @Translation("Did You Mean"),
 *   category = @Translation("Search"),
 * )
 * @phpcs:disable Drupal.Semantics.FunctionT.NotLiteralString
 */
final class SearchDidYouMean extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'text' => 'Did you mean: @suggestion?',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text to show'),
      '#description' => $this->t('@suggestion token will be replaced with results from the search.'),
      '#default_value' => $this->configuration['text'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $this->configuration['text'] = $form_state->getValue('text');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $config = $this->configuration;
    $searchAttributes = new Attribute();

    if ($config['text']) {
      $searchAttributes->setAttribute('text', $this->t($config['text'])->__toString());
    }

    return [
      '#theme' => 'swc_search_did_you_mean',
      '#search_attributes' => $searchAttributes,
      '#attached' => [
        'library' => [
          'search_web_components/components',
        ],
      ],
    ];
  }

}
