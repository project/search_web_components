<?php

namespace Drupal\search_web_components_spellcheck\EventSubscriber;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\search_api_decoupled\Event\SearchApiEndpointEvents;
use Drupal\search_api_decoupled\Event\SearchApiEndpointResultsAlter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Add additional endpoint config to the search api endpoint response.
 */
class EndpointEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      SearchApiEndpointEvents::SEARCH_RESULTS_ALTER => 'onSearchResultsAlter',
    ];
  }

  /**
   * Change search result response.
   *
   * @param \Drupal\search_api_decoupled\Event\SearchApiEndpointResultsAlter $event
   *   The event.
   */
  public function onSearchResultsAlter(SearchApiEndpointResultsAlter $event) {
    $search_response = $event->getResponse();
    $result = $event->getResult();

    if ($result->getExtraData('search_api_spellcheck')) {
      $suggestions = $result->getExtraData('search_api_spellcheck');
      $search_response['swc_spellcheck'] = [
        'collate' => $event->getResult()->getQuery()->getOption('search_api_spellcheck')['collate'],
        'collatedSuggestion' => $this->collateSuggestions($result->getQuery()->getOriginalKeys(), $suggestions),
        'suggestions' => $suggestions,
      ];
    }

    $event->setResponse($search_response);
  }

  /**
   * Create a collated search suggestion.
   *
   * @param string $searchTerm
   *   The original search term entered by the user.
   * @param array $spellcheckResults
   *   The spellcheck results from the search query.
   *
   * @return string
   *   The collated suggestion.
   */
  protected function collateSuggestions(string $searchTerm, array $spellcheckResults): string {
    $newSearchTerm = $spellcheckResults['collation'] ?? $searchTerm;

    if ($spellcheckResults['collation']) {
      $newSearchTerm = $spellcheckResults['collation'];
    }
    elseif (!empty($spellcheckResults['suggestions'])) {
      // Loop over the suggestions and replace the keys.
      foreach ($spellcheckResults['suggestions'] as $key => $values) {
        $newSearchTerm = str_ireplace($key, $values[0], $newSearchTerm);
      }
    }

    // Don't offer the identical search keys as "Did you mean".
    if ($newSearchTerm !== $searchTerm) {
      return $newSearchTerm;
    }

    return '';
  }

}
