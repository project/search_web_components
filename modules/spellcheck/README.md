# Installation

Spell check functionality depends on [#3499281 Make spellcheck available as an index processor](https://www.drupal.org/project/search_api_spellcheck/issues/3499281).
Until that issue is closed the patch from the issue will need to be applied.
