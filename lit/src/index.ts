import {SearchRoot} from './search-root/search-root.js';

import {FacetButton} from './facet-button/facet-button';
import {FacetCheckbox} from './facet-checkbox/facet-checkbox.js';
import {FacetDropdown} from './facet-dropdown/facet-dropdown.js';
import {FacetDropdownHtml} from './facet-dropdown-html/facet-dropdown-html.js';
import {SearchAppliedFacets} from './search-applied-facets/search-applied-facets.js';
import {SearchBox} from './search-box/search-box.js';
import {SearchInput} from './search-input/search-input.js';
import {SearchNoResultsMessage} from './search-no-results-message/search-no-results-message.js';
import {SearchResultElementDefault} from './search-result-element-default/search-result-element-default.js';
import {SearchResultElementRendered} from './search-result-element-rendered/search-result-element-rendered.js';
import {SearchResultSummary} from './search-result-summary/search-result-summary.js';
import {SearchResults} from './search-results/search-results.js';
import {SearchResultsPerPage} from './search-results-per-page/search-results-per-page.js';
import {SearchResultsSwitcher} from './search-results-switcher/search-results-switcher.js';
import {SearchSimplePager} from './search-simple-pager/search-simple-pager.js';
import {SearchSort} from './search-sort/search-sort.js';
import {BaseSearchElement} from './BaseSearchElement.js';
import {SearchDialogToggle} from './search-dialog-toggle/search-dialog-toggle.js';
import {SearchDialogPane} from './search-dialog-pane/search-dialog-pane.js';
import {SearchDidYouMean} from './search-did-you-mean/search-did-you-mean';

export default {
  BaseSearchElement,
  SearchRoot,
  FacetButton,
  FacetCheckbox,
  FacetDropdown,
  FacetDropdownHtml,
  SearchAppliedFacets,
  SearchBox,
  SearchDidYouMean,
  SearchInput,
  SearchNoResultsMessage,
  SearchResultElementDefault,
  SearchResultElementRendered,
  SearchResultSummary,
  SearchResults,
  SearchResultsPerPage,
  SearchResultsSwitcher,
  SearchSimplePager,
  SearchSort,
  SearchDialogPane,
  SearchDialogToggle,
};
