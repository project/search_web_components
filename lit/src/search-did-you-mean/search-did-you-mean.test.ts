import {$, expect} from '@wdio/globals';
import '../search-root/search-root.ts';
import './search-did-you-mean.ts';

describe('search-did-you-mean', () => {
  let elem, root, t: HTMLElement;

  beforeEach(() => {});

  it('default text should render', async () => {
    root = document.createElement('search-root');
    root.setAttribute('url', 'http://localhost:8181/api/search/mock-index');
    elem = document.createElement('search-did-you-mean');
    root.appendChild(elem);

    document.body.appendChild(root);
    const suggestionBefore = $('search-did-you-mean > .suggestion-before-text');
    await expect(suggestionBefore).toHaveText('Did you mean:');

    const suggestion = $('search-did-you-mean > button');
    await expect(suggestion).toHaveText('recipes');

    const suggestionAfter = $('search-did-you-mean > .suggestion-after-text');
    await expect(suggestionAfter).toHaveText('?');
  });

  it('provided text should render', async () => {
    root = document.createElement('search-root');
    root.setAttribute('url', 'http://localhost:8181/api/search/mock-index');
    elem = document.createElement('search-did-you-mean');
    elem.setAttribute('text', 'text before @suggestion text after');
    root.appendChild(elem);

    document.body.appendChild(root);
    const suggestionBefore = $('search-did-you-mean > .suggestion-before-text');
    await expect(suggestionBefore).toHaveText('text before');

    const suggestion = $('search-did-you-mean > button');
    await expect(suggestion).toHaveText('recipes');

    const suggestionAfter = $('search-did-you-mean > .suggestion-after-text');
    await expect(suggestionAfter).toHaveText('text after');
  });

  afterEach(() => {
    root.remove();
  });
});
