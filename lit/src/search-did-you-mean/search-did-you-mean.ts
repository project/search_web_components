import {html} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {BaseSearchElement} from '../BaseSearchElement';

export interface SearchResultsCounts {
  start: number;
  end: number;
  total: number;
}

/**
 * Displays the start, end, total, searchQuery, or time taken for the current search page.
 */
@customElement('search-did-you-mean')
export class SearchDidYouMean extends BaseSearchElement {
  /**
   * The text to display. `@suggestion` token is available and will be replaced with the values from the search.
   */
  @property()
  text = 'Did you mean: @suggestion?';

  /**
   * Run the current search with the corrected spelling.
   *
   * @param newQuery
   *   The new search term to use.
   */
  _updateQuery(newQuery: string | undefined) {
    const query = new URLSearchParams(this.context?.query);

    if (query) {
      query.set('q', newQuery ?? '');
    }

    this.getResults(query);
  }

  /**
   * Split the provided text attribute into parts around the suggestion.
   */
  _getParts() {
    // Add an @insert point so we know where to put suggestions.
    const tempText = this.text.replace(
      '@suggestion',
      '@suggestion@insert@suggestion'
    );
    const parts = tempText.split('@suggestion');

    let before = '';
    let after = '';
    const insertIndex = parts.findIndex((p) => p === '@insert');

    parts.forEach((value, index) => {
      if (index < insertIndex) {
        before += value;
      }

      if (index > insertIndex) {
        after += value;
      }
    });

    return {
      before: before,
      after: after,
    };
  }

  /**
   * @inheritDoc
   */
  override render() {
    if (!this.context?.response && !this.context?.response?.swc_spellcheck) {
      return null;
    }

    const suggestion =
      this.context?.response.swc_spellcheck?.collatedSuggestion;
    const {before, after} = this._getParts();

    if (!suggestion) {
      return null;
    }

    return html` <span class="suggestion-before-text">${before}</span>
      <button
        class="suggestion"
        @click=${() => {
          this._updateQuery(suggestion);
        }}
      >
        ${suggestion}
      </button>
      <span class="suggestion-after-text">${after}</span>`;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'search-did-you-mean': SearchDidYouMean;
  }
}
