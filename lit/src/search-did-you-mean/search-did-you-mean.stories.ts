import type {Meta, StoryObj} from '@storybook/web-components';
import {html} from 'lit';

import {SearchDidYouMean} from './search-did-you-mean';
import {SearchRoot} from '../search-root/search-root';

//@ts-expect-error required import.
const a = SearchDidYouMean;
//@ts-expect-error required import.
const b = SearchRoot;

const meta: Meta = {
  title: 'Components/search-did-you-mean',
  tags: ['autodocs'],
  component: 'search-did-you-mean',
  decorators: [
    (story) =>
      html`
        <search-root url="/mock/api/search/mock-index" defaultPerPage="10">
          ${story()}
        </search-root>
      `,
  ],
};

export default meta;
type Story = StoryObj;

export const Primary: Story = {
  render: ({text}) =>
    html`<search-did-you-mean text="${text}"></search-did-you-mean>`,
  args: {
    text: 'Did you mean @suggestion?',
  },
};
