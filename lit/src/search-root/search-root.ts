import {ContextRoot, provide} from '@lit/context';
import {SearchContext} from '../types';
import {searchContext} from '../SearchContext';
import {customElement, property, state} from 'lit/decorators.js';
import {LitElement} from 'lit';
import {BaseSearchElement} from '../BaseSearchElement';

/**
 * The root container for a component based search experience. All search components on the search page must be a child of this component to work correctly.
 */
@customElement('search-root')
export class SearchRoot extends LitElement {
  /**
   * The current search context.
   */
  @provide({context: searchContext})
  @state()
  context: SearchContext = {
    url: '',
    query: new URLSearchParams(window.location.search),
    responseReady: false,
    defaultPerPage: '10',
    updateUrl: true,
    resultDisplay: 'list',
    additionalParams: '',
    dialogOpen: false,
    dialogBreakpoint: 0,
    thirdPartySettings: {},
  };

  /**
   * The decoupled search api endpoint to query for results. Search query params added to this url will be used for the initial search if no search query params are present in the page url, they will also be added to the page url on search load, and can be removed by components.
   */
  @property()
  url = '';

  /**
   * A valid search query parameter string starting with '?'. These parameters will be added to all searches but will not be added to the page url.
   */
  @property()
  additionalParams = '';

  /**
   * The default number of results to show per page.
   */
  @property()
  defaultPerPage = '10';

  /**
   * The default result display.
   */
  @property()
  defaultResultDisplay: 'list' | 'grid' | string = 'list';

  /**
   * When set the page url is not updated with search query parameters.
   */
  @property({type: Boolean})
  noPageUrlUpdate = false;

  /**
   * The maximum screen width where the search-dialog-pane/button will act as a toggle. Set -1 to always apply.
   */
  @property({type: Number})
  dialogBreakpoint = -1;

  /**
   * The current context root.
   */
  root: ContextRoot = new ContextRoot();

  /**
   * Override the base Lit render root to disable shadow dom.
   *
   * @protected
   */
  protected override createRenderRoot(): HTMLElement | DocumentFragment {
    this.style.display = 'block';
    return this;
  }

  override async connectedCallback() {
    super.connectedCallback();

    await this.doSearch();
  }

  async doSearch(customQuery?: URLSearchParams) {
    const context = {...this.context};

    let initialQuery: URLSearchParams | undefined;
    if (this.url) {
      const [url, query] = this.url.split('?');
      if (url.startsWith('/')) {
        context.url = new URL(url, document.baseURI).href;
      } else {
        context.url = url;
      }

      if (query) {
        initialQuery = new URLSearchParams(query);
      }
    }
    context.defaultPerPage = this.defaultPerPage;
    context.resultDisplay = this.defaultResultDisplay;
    context.dialogBreakpoint = this.dialogBreakpoint;

    if (this.noPageUrlUpdate) {
      context.updateUrl = false;
    }

    // Initialize the query to empty if it's not set or if we want to ignore parameters in the url.
    if (!context.query || this.noPageUrlUpdate) {
      context.query = new URLSearchParams();
    }

    // Only use the initial query if the current page doesn't have any search query parameters present.
    if (initialQuery) {
      let useInitial = true;
      const queryTags = ['q', 'page', 'limit', 'sort', 'order'];
      context.query.forEach((value, key) => {
        if ((queryTags.includes(key) && value !== '') || key.startsWith('f[')) {
          useInitial = false;
        }
      });
      if (context.query.size === 0 || useInitial) {
        context.query = initialQuery;
        this.context.query = initialQuery;
      }
    }

    if (customQuery) {
      context.query = customQuery;
      this.context.query = customQuery;
    }

    // Merge the current query with additional params.
    context.additionalParams = this.additionalParams;
    let searchQuery = context.query;
    if (this.additionalParams) {
      searchQuery = new URLSearchParams(
        new URLSearchParams(this.additionalParams).toString() +
          '&' +
          searchQuery.toString()
      );
    }

    // @todo find a better way to do this.
    const eventPre = new CustomEvent('swc-pre-search', {
      bubbles: true,
      composed: true,
    });

    // @ts-expect-error
    eventPre.data = {
      query: searchQuery,
      searchQuery: searchQuery,
      context: context,
    };
    this.dispatchEvent(eventPre);

    context.response = await BaseSearchElement.doSearch(
      context.url ?? '',
      searchQuery
    );
    context.responseReady = true;

    // Update the url to match the state of the first query on load.
    if (context.updateUrl) {
      const u = new URL(window.location.href);
      u.search = context.query.toString();
      window.history.pushState(null, '', u.toString());
    }

    // @todo find a better way to do this.
    const eventPost = new CustomEvent('swc-post-search', {
      bubbles: true,
      composed: true,
    });

    // @ts-expect-error
    eventPost.data = {
      query: searchQuery,
      searchQuery: searchQuery,
      context: context,
    };
    this.dispatchEvent(eventPost);

    this.context = context;
    this.addEventListener('swc-update-context', (e) => {
      //@ts-expect-error This custom event has this property but isn't registered correctly in all tooling.
      this.context = {...this.context, ...e.context};
    });

    if (this.context.response?.search_results_count === 0) {
      this.classList.add('no-results');
    } else {
      this.classList.add('has-results');
    }
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'search-root': SearchRoot;
  }
}
